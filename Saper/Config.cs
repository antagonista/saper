﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Saper
{
    public partial class Config : Form
    {

        Form1 OknoGlowne;
        
        public Config(Form1 form1)
        {

            OknoGlowne = form1;
  
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // zapisz

            // sprawdzamy czy miny sie zmieszcza

            try
            {
                int iloscPol = Convert.ToInt32(textBox1.Text) * Convert.ToInt32(textBox2.Text);
                int iloscMin = Convert.ToInt32(textBox3.Text);

                if ((iloscPol - iloscMin) >= 10)
                {
                    //OknoGlowne.POZIOMO = Convert.ToInt32(textBox1.Text);
                    //OknoGlowne.PIONOWO = Convert.ToInt32(textBox2.Text);

                    OknoGlowne.POZIOMO = Convert.ToInt32(textBox2.Text);
                    OknoGlowne.PIONOWO = Convert.ToInt32(textBox1.Text);

                    OknoGlowne.MINY = Convert.ToInt32(textBox3.Text);

                    Close();
                }
                else
                {
                    MessageBox.Show("Ilość pol do gry powinna byc min. o 10 wieksza od ilosci min!", "Error :(");
                }


            }
            catch
            {
                MessageBox.Show("Możesz podać tylko liczby!\nMin nie może być więcej niż pól do gry (min o 10 mniej min)!","Error :(");
            }

            




        }

        private void button2_Click(object sender, EventArgs e)
        {
            // reset

            

            textBox1.Text = "16";
            textBox2.Text = "16";
            textBox3.Text = "40";

        }
    }
}
