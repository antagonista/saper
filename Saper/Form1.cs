﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Saper
{
    public partial class Form1 : Form
    {
        class Pole
        {
            public bool mina;            // czy jest tu mina?
            public bool sprawdzone; // gracz sprawdzil to pole?
            public bool flaga;           // gracz postawil tu flagae?
            public int sasiedzi;   // liczba min w sasiedztwie
        }

        public int POZIOMO = 16; // pola w wierszu
        public int PIONOWO = 16; // pola w kolumnie
        public int MINY = 40;  // ile min na planszy
        Pole[,] Plansza;
        int ukryte;

        // zmienne pomocnicze
        int ileMin = 0;
        System.Diagnostics.Stopwatch sw;
        public int najlepszyWynik;
        public bool ustanowionyRekord = false;



        void losujMiny()
        {

            ukryte = POZIOMO * PIONOWO - MINY;

            // czyscimy plansze

            for (int y = 0; y < PIONOWO; y++)

                for (int x = 0; x < POZIOMO; x++)
                {

                    Plansza[y, x].sprawdzone = false;

                    Plansza[y, x].mina = false;

                    Plansza[y, x].flaga = false;

                }

            // losujemy miny

            Random r = new Random();

            for (int n = 0; n < MINY; n++)
            {

                int x, y;

                do
                {

                    x = r.Next(POZIOMO);

                    y = r.Next(PIONOWO);

                } while (Plansza[y, x].mina);

                Plansza[y, x].mina = true;

            }

            // obliczamy liczbe min w sasiedztwie

            for (int y = 0; y < PIONOWO; y++)

                for (int x = 0; x < POZIOMO; x++)
                {

                    int c = 0;

                    for (int yy = y - 1; yy <= y + 1; yy++)

                        for (int xx = x - 1; xx <= x + 1; xx++)

                            if (xx >= 0 && xx < POZIOMO)

                                if (yy >= 0 && yy < PIONOWO)

                                    if (Plansza[yy, xx].mina) c++;

                    if (Plansza[y, x].mina) c--;

                    Plansza[y, x].sasiedzi = c;

                }

        }


        
        public Form1()
        {

            OtworzConfig();

            InitializeComponent();

            //Width = (int)Math.Round((double)POZIOMO * 16);
            //Height = (int)Math.Round((double)PIONOWO * 16);


        }

        void odkryj(int x, int y)
        {

            for (int yy = y - 1; yy <= y + 1; yy++)

                for (int xx = x - 1; xx <= x + 1; xx++)

                    if (xx >= 0 && xx < POZIOMO)

                        if (yy >= 0 && yy < PIONOWO)

                            if (!Plansza[yy, xx].sprawdzone)
                            {

                                Plansza[yy, xx].sprawdzone = true;

                                ukryte--;

                                if (Plansza[yy, xx].sasiedzi == 0)

                                    odkryj(xx, yy);

                            }

        }


        private void Form1_Load(object sender, EventArgs e)
        {

            // tworzymy plansze
            Plansza = new Pole[PIONOWO, POZIOMO];

            for (int y = 0; y < PIONOWO; y++)
            {
                for (int x = 0; x < POZIOMO; x++)
                {
                    Plansza[y, x] = new Pole();
                }
            }
            losujMiny();

            Text = LicznikMin();

            sw = System.Diagnostics.Stopwatch.StartNew();

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Font f = new Font("Arial", 12, FontStyle.Bold);

            //int maxPion = PIONOWO * 16;
            //int maxPozi = POZIOMO * 16;

            int cosTakiego = PIONOWO * POZIOMO;

            //g.DrawLine(Pens.Gray, 0, 256, 256, 256);
            //g.DrawLine(Pens.Gray, 0, cosTakiego, cosTakiego, cosTakiego);
            //g.DrawLine(Pens.Gray, 256, 0, 256, 256);
            //g.DrawLine(Pens.Gray, cosTakiego, 0, cosTakiego, cosTakiego);


            // RYSOWANIE SAMEJ SIARKI
            for (int y = 0; y < POZIOMO; y++)
            {
                g.DrawLine(Pens.Gray, y * 16, 0, y * 16, PIONOWO * 16);
            }

            for (int x = 0; x < PIONOWO; x++)
            {
                g.DrawLine(Pens.Gray, 0, x * 16, 16 * POZIOMO, x * 16);
            }

            // ustawianie szerokosci okna 
            Width = (POZIOMO * 16)+9;
            Height = (PIONOWO * 16)+30;


            // END RYSOWANIE SAMEJ SIARKI


            // RYSOWANIE ZNACZNIKOW

            for (int y = 0; y < PIONOWO; y++)
            {

                //g.DrawLine(Pens.Gray, y * 16, 0, y * 16, 256);

                // rysuje pionowe linie
                //g.DrawLine(Pens.Gray, y * 16, 0, y * 16, PIONOWO*16);
                //g.DrawLine(Pens.Gray, 0, y * 16, 16 * POZIOMO, y * 16);

                //g.DrawLine(Pens.Gray, 0, y * 16, 256, y * 16);

                //g.DrawLine(Pens.Gray, 0, y * PIONOWO, cosTakiego, y * PIONOWO);

                String s;

                Brush b;

                

                for (int x = 0; x < POZIOMO; x++)
                {

                    //g.DrawLine(Pens.Gray, 0, x * 16, 16 * POZIOMO, x*16);


                    if (Plansza[y, x].sprawdzone)
                    {

                        s = Convert.ToString(Plansza[y, x].sasiedzi);

                        b = Brushes.DarkRed;

                        switch (Plansza[y, x].sasiedzi)
                        {

                            case 0: s = " "; break;

                            case 1: b = Brushes.Blue; break;

                            case 2: b = Brushes.Green; break;

                            case 3: b = Brushes.Orange; break;

                            case 4: b = Brushes.Red; break;

                        }

                    }

                    else if (Plansza[y, x].flaga)
                    {

                        s = "+";

                        b = Brushes.Crimson;

                    }

                    else
                    {

                        s = "#";

                        b = Brushes.Black;

                    }
                    g.DrawString(s, f, b, x * 16, y * 16);

                }

            }

            // END RYSOWANIE ZNACZNIKOW


            
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {

            int x = (e.X - ClientRectangle.Left) / 16;
            //int x = (e.X - ClientRectangle.Left) / POZIOMO;
            int y = (e.Y - ClientRectangle.Top) / 16;
            //int y = (e.Y - ClientRectangle.Top) / PIONOWO;

            if (!Plansza[y, x].sprawdzone)
            {

                if (e.Button == MouseButtons.Left && !Plansza[y, x].flaga)
                {

                    CzyscFlagi();

                    // koniec gry - trafienie na mine
                    if (Plansza[y, x].mina)
                    {

                        sw.Stop();

                        DialogResult dr =

                        MessageBox.Show(this,

                        "Bummmm! Już po Tobie! \nChcesz zagrać jeszcze raz? \n\nCzas gry: " + PrzeliczCzas(sw.Elapsed.Seconds) + ZarzadzanieWynikami(false),

                        "Koniec gry", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                        sw.Reset();
                        

                        // koniec faktyczny
                        if (dr == DialogResult.No) Close();

                        // a jak nie to gramy dalej
                        Text = LicznikMin();
                        losujMiny();

                        sw.Start();

                        Refresh();

                        return;

                    }

                    ukryte--;

                    Plansza[y, x].sprawdzone = true;

                    // dobre trafienie - nie w mine
                    if (Plansza[y, x].sasiedzi == 0)

                        odkryj(x, y);


                    // zwyciestwo
                    if (ukryte == 0)
                    {

                        sw.Stop();

                        DialogResult dr =

      MessageBox.Show(this,

      "Hura! Zwycięstwo! \nChcesz zagrać jeszcze raz? \n\nCzas gry: " + PrzeliczCzas(sw.Elapsed.Seconds) + ZarzadzanieWynikami(true),

      "Koniec gry", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                        sw.Reset();
                        

                        if (dr == DialogResult.No) Close();



                        Text = LicznikMin();
                        losujMiny();

                        sw.Start();

                        Refresh();

                        return;

                    }

                }

                else if (e.Button == MouseButtons.Right)
                
                    
                    ileMin = IleMin();

                    // ograniczenia co do mozliwosci max stawiania flag na planszy (nie wiecej niz min)

                    if (Plansza[y, x].flaga == false && ileMin >= 1) 
                    {
                        //Plansza[y, x].flaga = !Plansza[y, x].flaga; 
                        Plansza[y, x].flaga=!Plansza[y, x].flaga;
                    }
                    else if (ileMin >= 0 && Plansza[y, x].flaga == true) Plansza[y, x].flaga = !Plansza[y, x].flaga; 
                    
                    //Plansza[y, x].flaga = !Plansza[y, x].flaga;

                    // licznik min
                    Text = LicznikMin();
                    Refresh();


            }

        }

        
        string PrzeliczCzas(int sekundy)
        {

            TimeSpan t = TimeSpan.FromSeconds(sekundy);
            
            string answer = string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);

            return answer;
        }

        string LicznikMin()
        {

            CzyscFlagi();

            int licznik = 0;

            foreach (Pole rekord in Plansza)
            {
                if (rekord.flaga == true) licznik++;
            }

            int licznikMin = MINY - licznik;

            return "Saper: pozostalo "+licznikMin.ToString()+" flag";
        }

        int IleMin()
        {

            CzyscFlagi();

            int licznik = 0;

            foreach (Pole rekord in Plansza)
            {
                if (rekord.flaga == true) licznik++;
            }

            int licznikMin = MINY - licznik;

            return licznikMin;
        }

        string ZarzadzanieWynikami(bool wygrana = true)
        {

            string mns = "\n\n";
            int wynikGracza;

            if (wygrana == false)
            {
                wynikGracza = 0;
            }
            else
            {
                wynikGracza = sw.Elapsed.Seconds;
            }


            if (najlepszyWynik == 0 && wygrana == true && ustanowionyRekord == false)
            {
                mns = mns + "Gratulacje! osiągnales najlepszy wynik!\nTwoj wynik: " + wynikGracza + "\nStary rekord: " + najlepszyWynik;
                najlepszyWynik = wynikGracza;
                ustanowionyRekord = true;
            }
            else if (najlepszyWynik >= wynikGracza && wygrana == true)
            {
                mns = mns + "Gratulacje! osiągnales najlepszy wynik!\nTwoj wynik: " + wynikGracza + "\nStary rekord: " + najlepszyWynik;
                najlepszyWynik = wynikGracza;
            }
            else
            {
                mns = mns + "Twoj wynik: " + wynikGracza;

                if (najlepszyWynik == 0) mns = mns + "\nNajlepszy wynik: brak";
                else mns = mns + "\nNajlepszy wynik: " + najlepszyWynik;
            }

            /*
            if (najlepszyWynik >= wynikGracza && wygrana == true && najlepszyWynik != 0)
            {
                mns = mns + "Gratulacje! osiągnales najlepszy wynik!\nTwoj wynik: " + wynikGracza + "\nStary rekord: "+najlepszyWynik;
                najlepszyWynik = wynikGracza;
            }
            else if (najlepszyWynik==0 && wygrana == true)
            {
                mns = mns + "Gratulacje! osiągnales najlepszy wynik!\nTwoj wynik: " + wynikGracza + "\nStary rekord: " + najlepszyWynik;
                najlepszyWynik = wynikGracza;
            }
            else
            {
                mns = mns + "Twoj wynik: " + wynikGracza;

                if (najlepszyWynik == 0) mns = mns + "\nNajlepszy wynik: brak";
                else mns = mns + "\nNajlepszy wynik: " + najlepszyWynik;

            }
            */
            return mns;

        }

        void OtworzConfig()
        {
            //Visible = false;
            Config OknoConfig = new Config(this);
            OknoConfig.ShowDialog();
            //Visible = true;
        }

        void CzyscFlagi()
        {
            foreach (Pole rekord in Plansza)
            {
                if (rekord.sprawdzone == true && rekord.flaga == true) rekord.flaga = false;
            }
        }

    }
}
